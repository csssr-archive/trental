trental
=======

Trental Presentation (iOS7)


##### Макеты

1-ая часть макетов (6 слайдов): https://www.dropbox.com/sh/3qd73u8jjnvciul/AACi99LT3u9qhgObtspe0XjWa <br>
2-ая часть (4 слайда): https://www.dropbox.com/sh/p8bpnzi9uch7svp/AADb50niTTBOq8DyJfJOBkvHa 
Шрифт https://www.dropbox.com/s/49cme2nicgm3ws3/fonts.rar

##### Технические требования

- Поддержка Webkit
- Поддержка в частности iOS7 (Safari)
- Соответствие техническим требованиям https://www.dropbox.com/s/m0vwwepekf1d7bi/technical.pdf
- Каждый слайд должен быть в отдельном sequence
- Данная презентация будет объединяться потом с другими презентациями в некую последовательность слайдов для демонстрации, надо это предусмотреть в верстке (необходимо сделать уникальные стили, чтобы по максимуму пресечь возможные пересечения по стилям)


Пример работающей презентации тут https://www.dropbox.com/s/6kfvgo65b4tvwxa/sample.zip
